import { Message, PermissionResolvable } from 'discord.js'

export interface Command {
	name: string
	permission: PermissionResolvable
	adminOverride?: boolean
	guildOnly?: boolean
	args?: number
	description?: string
	usage?: string
	run: (message: Message, args: string[]) => void
}
