import { Client, Intents, Constants } from 'discord.js'
import directMessage from './events/directMessage'
import grammarCheck from './events/grammarCheck'
import messageCreate from './events/messageCreate'
import messageReactionAdd from './events/messageReactionAdd'
import ready from './events/ready'

console.log('Bot starting')

const client = new Client({
	intents: [
		Intents.FLAGS.GUILDS,
		Intents.FLAGS.GUILD_MESSAGES,
		Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
		Intents.FLAGS.DIRECT_MESSAGES
	],
	partials: Object.values(Constants.PartialTypes) // Listen to partial events e.g. reactions to messages that are not cached
})

// Event listeners
ready(client)
messageCreate(client)
messageReactionAdd(client)
grammarCheck(client)
directMessage(client)

export default client
