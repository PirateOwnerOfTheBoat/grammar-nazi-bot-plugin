import { Message, MessageEmbed } from 'discord.js'
import { DateTime } from 'luxon'
import Mistake from '../../Models/Mistake'

// TODO: Refactor and describe - Misko
async function getMistakesPerUser(from: DateTime, guildId: string): Promise<UserMistakeCount[]> {
	const stats = await Mistake.query()
		.where('message_timestamp', '>', from.toSQL({ includeOffset: false }))
		.andWhere('guild_id', '=', guildId)
		.count('author_id as mistakes')
		.groupBy('author_id')
		.select('author_id')

	let statistics: UserMistakeCount[] = []
	for (const mistake of stats) {
		statistics.push({ userId: mistake.authorId, mistakeCount: mistake.$extras.mistakes })
	}

	return statistics
}

export default {
	async replyWithStatistics(message: Message, days: number) {
		//@ts-expect-error
		const mistakes = (await getMistakesPerUser(DateTime.now().minus({ days: days }), message.guildId)).sort(
			(a, b) => b.mistakeCount - a.mistakeCount
		)

		let text: string = ''
		for (const mistake of mistakes) {
			//@ts-expect-error guildOnly
			const member = await message.guild.members.fetch(mistake.userId)
			text += `${member} ${mistake.mistakeCount}\n`
		}

		const embed = new MessageEmbed().setTitle(`${days} days stats`).setDescription(text).setColor('#FF0000')

		await message.reply({ embeds: [embed] })
	}
}
