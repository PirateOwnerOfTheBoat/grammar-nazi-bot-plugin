import GrammarEmoji from '@/Misko/NaziBot/Models/GrammarEmoji'
import LogChannel from '@/Misko/NaziBot/Models/LogChannel'
import Mistake from '@/Misko/NaziBot/Models/Mistake'
import Prefix from '@/Misko/NaziBot/Models/Prefix'
import grammarNazi from '../grammarNazi'
import { Guild, Message, MessageEmbed, NewsChannel, TextChannel } from 'discord.js'
import { defaultPrefix } from '../config.json'
import { DateTime } from 'luxon'

// NOTE: Very useful
// const sleep = (ms) => {
// 	return new Promise((resolve) => setTimeout(resolve, ms))
// }

async function loadMessagesFromChannel(channel: TextChannel | NewsChannel, messagesNum: number): Promise<Message[]> {
	// Message array
	let messages: Message[] = []
	// Last message Snowflake to fetch from
	let lastMessage: string | undefined

	// Loop is fetching 100 messages at a time (API limit)
	while (messages.length < messagesNum) {
		// Fetched messages that were sent after lastMessage Snowflake
		const messagesList = await channel.messages.fetch({
			limit: Math.min(100, messagesNum - messages.length),
			before: lastMessage
		})

		// Set last message Snowflake
		lastMessage = messagesList.lastKey()

		// Push fetched messages to array
		messages.push(...messagesList.values())

		// If last messages were loaded break
		if (messagesList.size !== 100) break
	}

	return messages
}

async function handleMessage(message: Message, checkDatabase: boolean = false): Promise<void> {
	const prefix: string = (await Prefix.findBy('guildId', message.guildId))?.prefix || defaultPrefix
	// If message author is bot or it's a command or it's sent outside of a guild don't check it
	if (message.author.bot || message.content.startsWith(prefix) || !message.channel.type.startsWith('GUILD')) return

	console.log(`Checking message ${message.id} for gramatical errors`)

	// Checking message for gramatic errors
	const reply = grammarNazi.checkMessage(message)
	const suggestionReply = grammarNazi.checkSuggestions(message)

	// Get nazi log channel from database
	const logChannel = await LogChannel.findBy('guildId', message.guildId)
	// If this guild doesn't have nazi log channel saved in database return
	if (!logChannel) return

	// Fetch nazi log channel
	const channel = await message.guild?.channels.fetch(logChannel.channelId)
	// If it doesn't exist return
	if (!channel || !channel.isText()) return

	// Database check
	if (checkDatabase) {
		// Get mistake from database
		const mistake = await Mistake.findBy('messageId', message.id)

		// If mistake is no longer detected delete it
		if (mistake && !reply) {
			await mistake.delete()
			return
		}

		// If mistake was found and it can be updated
		if (mistake && reply && mistake.mistake !== reply) {
			// Update mistake
			await mistake.merge({ mistake: reply }).save()

			// And log embed to grammar nazi log
			const embed = new MessageEmbed()
				.setColor('#FF0000')
				.setDescription(reply)
				.addField('Updated sprava', `${message.content}\nv <#${message.channel.id}> od ${message.author}`)

			await channel.send({ embeds: [embed] })

			return
		}

		if (mistake) return
	}
	// If gramatic error wasn't found return
	if (!reply && !suggestionReply) return

	// React and save to database only if mistake outside of suggestions was found
	if (reply) {
		// Get emoji for this guild from database
		const grammarNaziEmoji = await GrammarEmoji.findBy('guildId', message.guildId)
		// If emoji doesn't exist in database return
		if (!grammarNaziEmoji) return

		// Fetch discord emoji
		const emoji = await message.guild?.emojis.fetch(grammarNaziEmoji.emojiId)
		// If emoji in database no longer return
		if (!emoji) return

		// Else react to message with grammar nazi emoji
		await message.react(emoji.toString())

		// Save mistake to database for statistics
		await Mistake.create({
			//@ts-expect-error checked channel.type.startsWith('GUILD')
			guildId: message.guildId,
			messageId: message.id,
			authorId: message.author.id,
			messageContent: message.content,
			mistake: reply,
			messageTimestamp: DateTime.fromJSDate(message.createdAt),
			channelId: message.channelId
		})
	}

	// And log embed to grammar nazi log
	const embed = new MessageEmbed()
		.setColor('#FF0000')
		.setDescription(`${reply ? reply : ''}\n${suggestionReply ? 'Suggestions: ' + suggestionReply : ''}`)
		.addField('Sprava', `${message.content}\nv <#${message.channel.id}> od ${message.author}`)

	await channel.send({ embeds: [embed] })
}

export default {
	async handleMessage(message: Message): Promise<void> {
		await handleMessage(message)
	},
	async checkGuild(guild: Guild, messagesNum: number): Promise<void> {
		// Fetch all channels
		const channels = await guild.channels.fetch()

		console.log(`Running checks in ${channels.size} channels...`)
		// For every channel in guild
		for (const channel of channels.values()) {
			// That is text or news channel
			if (!channel.isText()) continue

			console.log(`Checking channel ${channel.name}`)
			// Load messagesNum messages from channel
			const messages = await loadMessagesFromChannel(channel, messagesNum)

			// Check every message for gramatical errors
			for (const message of messages.values()) {
				await handleMessage(message, true)
			}
		}
		console.log('Checks done.')
	}
}
