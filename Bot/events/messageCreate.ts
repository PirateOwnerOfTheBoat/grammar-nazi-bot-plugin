import Prefix from '@/Misko/NaziBot/Models/Prefix'
import { commands } from '../command'
import { Client, Message } from 'discord.js'
import { defaultPrefix } from '../config.json'

export default (client: Client): void => {
	client.on('messageCreate', async (message: Message) => {
		// Get prefix from database or use default prefix
		const prefix = (await Prefix.findBy('guildId', message.guildId))?.prefix || defaultPrefix
		// If message is partial fetch it
		if (message.partial) await message.fetch()
		// Filter out bots and normal messages
		if (!(message.content.startsWith(prefix) && !message.author.bot)) return

		// Split message to arguments without prefix
		const args = message.content.slice(prefix.length).split(/ +/)
		// Get name of command from first argument and remove it
		const commandName = args.shift()?.toLowerCase()

		// Find command
		const command = commands.find((command) => command.name === commandName)

		// If command doesn't exist return
		if (!command) return
		// If required arguments are not supplied return
		if (command.args && command.args < args.length) {
			await message.reply({ content: 'Missing arguments.' })
			return
		}

		// If message was sent in guild
		if (message.channel.type.startsWith('GUILD')) {
			// If user doesn't have neccessary permissions
			if (!message.member?.permissions.has(command.permission, command.adminOverride)) {
				await message.reply({ content: `You don't have neccessary permissions to run this command.` })
				return
			}
			console.log(`Executing command: ${commandName}`)
			try {
				await command.run(message, args)
			} catch (error) {
				console.error(error)
				await message.reply({ content: 'Error' })
			}
			return
		}

		// If command is guild only and message wasn't sent in guild
		if (command.guildOnly) {
			await message.reply({ content: 'This command can only be executed in guilds.' })
			return
		}
		console.log(`Executing command: ${commandName}`)
		try {
			await command.run(message, args)
		} catch (error) {
			console.error(error)
			await message.reply({ content: 'Error' })
		}
	})
}
