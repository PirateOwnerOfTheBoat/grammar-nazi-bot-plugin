import GrammarEmoji from '@/Misko/NaziBot/Models/GrammarEmoji'
import LogChannel from '@/Misko/NaziBot/Models/LogChannel'
import Report from '@/Misko/NaziBot/Models/Report'
import { Client, MessageEmbed, MessageReaction, User } from 'discord.js'

export default (client: Client): void => {
	client.on('messageReactionAdd', async (reaction: MessageReaction, user: User) => {
		// Filter bots out
		if (user.bot) return

		// Fetch partial reactions
		if (reaction.partial) await reaction.fetch()
		// Return if reaction wasn't in guild
		if (!reaction.message.channel.type.startsWith('GUILD')) return

		const grammarNaziEmoji = await GrammarEmoji.findBy('guildId', reaction.message.guildId)
		if (!grammarNaziEmoji) return

		const emoji = await reaction.message.guild?.emojis.fetch(grammarNaziEmoji.emojiId)
		if (!emoji) return

		// If reaction emoji is not grammar nazi emoji return
		if (reaction.emoji.id !== emoji.id) return

		// Get log channel from database
		const logChannelId = (await LogChannel.findBy('guildId', reaction.message.guildId))?.channelId
		// If log channel doesn't exist
		if (!logChannelId) return

		// Resolve nazi log channel
		const channel = await reaction.message.guild?.channels.fetch(logChannelId)
		// Check if channel exists
		if (!channel || !channel.isText()) return

		// Check if report for current reaction already exist
		const report = await Report.findBy('messageId', reaction.message.id)
		if (report) return

		await Report.create({
			messageContent: reaction.message.content || '',
			authorId: user.id,
			userId: reaction.message.author?.id,
			//@ts-expect-error checked reaction.message.channel.type.startsWith('GUILD')
			guildId: reaction.message.guildId,
			messageId: reaction.message.id,
			channelId: reaction.message.channelId
		})

		const embed: MessageEmbed = new MessageEmbed()
			.setColor('#FF0000')
			.setDescription(`Nahlasena chyba: ${user}`)
			.addField(
				'Sprava',
				`${reaction.message.content}\nv <#${reaction.message.channel.id}> od ${reaction.message.author}`
			)

		await channel.send({ embeds: [embed] })
	})
}
