import { Client, Message, MessageEmbed } from 'discord.js'
import { naziLogChannelId, mainGuildId } from '@/Misko/NaziBot/Bot/config.json'
import grammarUtils from '../grammarNazi'
import Mistake from '../../Models/Mistake'
import { DateTime } from 'luxon'

export default (client: Client): void => {
	client.on('messageCreate', async (message: Message) => {
		// If message author is bot return
		if (message.author.bot) return
		// If message is not direct message return
		if (message.channel.type !== 'DM') return

		console.log(`Checking DM from ${message.author.id} (${message.author.tag})`)

		// Check message for gramatical errors
		const reply = grammarUtils.checkMessage(message)

		// Fetch main guild log channel
		const channel = await (await client.guilds.fetch(mainGuildId)).channels.fetch(naziLogChannelId)
		if (!channel?.isText()) return

		let replyEmbed: MessageEmbed
		let logEmbed: MessageEmbed

		// If gramatical errors were found
		if (reply) {
			replyEmbed = new MessageEmbed().setColor('#FF0000').setTitle('Chyba!').setDescription(reply)
			logEmbed = new MessageEmbed()
				.setColor('#FF0000')
				.setDescription(reply)
				.addField('Sprava', `${message.content}\nv DM od ${message.author}`)

			await Mistake.create({
				authorId: message.author.id,
				messageContent: message.content,
				mistake: reply,
				messageTimestamp: DateTime.fromJSDate(message.createdAt),
				messageId: message.id
			})
		} else {
			replyEmbed = new MessageEmbed().setColor('GREEN').setTitle('Nenasiel som chyby')
			logEmbed = new MessageEmbed()
				.setColor('GREEN')
				.addField('Sprava v DM', `${message.content} od ${message.author}`)
				.setFooter({ text: 'Nenasiel som chybu' })
		}

		// Log message to main guild log channel and reply to user
		await message.reply({ embeds: [replyEmbed] })
		await channel.send({ embeds: [logEmbed] })
	})
}
