import GrammarEmoji from '@/Misko/NaziBot/Models/GrammarEmoji'
import { Message } from 'discord.js'
import { Command } from '../../types/Command'

export const GrammarEmojiCommand: Command = {
	name: 'grammaremoji',
	args: 1,
	guildOnly: true,
	permission: 'ADMINISTRATOR',
	adminOverride: true,
	description: 'Sets grammar emoji for current guild by emoji id',
	usage: '<emojiId>',
	run: async (message: Message, args: string[]) => {
		// Fetch user supplied emoji
		const emoji = await message.guild?.emojis.fetch(args[0])
		// If emoji with that id doesn't exist in this guild return
		if (!emoji) {
			await message.reply({ content: `Emoji with this id doesn't exist.` })
			return
		}

		// Check if grammar nazi emoji exists in database
		// If not create new else update existing one
		// @ts-expect-error checked in messageCreateEvent
		await GrammarEmoji.updateOrCreate({ guildId: message.guildId }, { emojiId: args[0] })

		await message.reply({ content: `Grammar nazi emoji changed to ${emoji}.` })
	}
}
