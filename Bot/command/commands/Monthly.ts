import { Message } from 'discord.js'
import { Command } from '../../types/Command'
import statsUtils from '../../utils/statsUtils'

export const Monthly: Command = {
	name: 'monthly',
	args: 0,
	guildOnly: true,
	permission: 'ADMINISTRATOR',
	adminOverride: true,
	description: 'Mistake statistics for 30 days',
	run: async (message: Message) => {
		await statsUtils.replyWithStatistics(message, 30)
	}
}
