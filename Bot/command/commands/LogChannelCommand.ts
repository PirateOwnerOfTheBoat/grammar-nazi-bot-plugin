import LogChannel from '@/Misko/NaziBot/Models/LogChannel'
import { Message } from 'discord.js'
import { Command } from '../../types/Command'

export const LogChannelCommand: Command = {
	name: 'setlog',
	args: 0,
	guildOnly: true,
	permission: 'ADMINISTRATOR',
	adminOverride: true,
	description: 'Sets current channel as log channel for current guild',
	run: async (message: Message) => {
		// If guild is still using default log channel create new log channel
		// or else update already existing log channel
		// @ts-expect-error checked in messageCreateEvent
		await LogChannel.updateOrCreate({ guildId: message.guildId }, { channelId: message.channelId })

		await message.reply({ content: `Log channel changed to \`${message.channelId}\`.` })
	}
}
