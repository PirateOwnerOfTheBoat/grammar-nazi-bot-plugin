import { Message, MessageEmbed } from 'discord.js'
import { Command } from '../../types/Command'
import { commands } from '../index'

export const Help: Command = {
	name: 'help',
	args: 0,
	guildOnly: false,
	adminOverride: true,
	permission: 'ADMINISTRATOR',
	description: 'Sends help message',
	run: async (message: Message, args) => {
		if (args.length < 1) {
			let helpText = ''
			for (const command of commands) {
				if (command.description) {
					helpText += `\`${command.name}\`${command.description ? ' ' + command.description : ''}\n`
				}
			}

			const embed = new MessageEmbed().setTitle('Help').setDescription(helpText)

			await message.reply({ embeds: [embed] })

			return
		}

		const command = commands.find((command) => command.name === args[0])
		if (!command) {
			await message.reply({ content: 'Command not found.' })
			return
		}

		const embed = new MessageEmbed()
			.setTitle(command.name)
			.setDescription(
				`${command.description}${command.usage ? '\nUsage: `' + command.name + ' ' + command.usage + '`' : ''}`
			)

		await message.reply({ embeds: [embed] })
	}
}
