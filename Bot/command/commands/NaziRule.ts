import { Command } from '../../types/Command'
import { Message } from 'discord.js'
import fs from 'fs'
import { DateTime } from 'luxon'

export const NaziRule: Command = {
	name: 'nazirule',
	guildOnly: true,
	permission: 'ADMINISTRATOR',
	adminOverride: true,
	description: 'Creates new temporary rule',
	usage: '"<mistake>", "<category>"',
	run: async (message: Message) => {
		// Create array from message arguments in ""
		const params = Array.from(message.content.matchAll(/"(.*?)"/gm), (m) => m[1])
		// If not enough arguments provided return
		if (params.length < 2) {
			message.reply({ content: 'Missing arguments.' })
			return
		}

		// Read suggestions from file
		let suggestions = JSON.parse(
			fs.readFileSync('plugins/Misko/NaziBot/Bot/suggestions.json', { encoding: 'utf-8', flag: 'r' })
		)

		// Add new suggestion to list
		suggestions.push({
			mistake: params[0],
			category: params[1],
			userId: message.author.id,
			guildId: message.guildId,
			timestamp: DateTime.now().toMillis()
		})

		message.reply({ content: `Chyba: \`${params[0]}\` \`${params[1]}\`` })

		// Save suggestions
		fs.writeFileSync('plugins/Misko/NaziBot/Bot/suggestions.json', JSON.stringify(suggestions), {
			encoding: 'utf-8',
			flag: 'w'
		})
	}
}
