import { Message } from 'discord.js'
import { Command } from '../../types/Command'
import statsUtils from '../../utils/statsUtils'

export const Yearly: Command = {
	name: 'yearly',
	args: 1,
	guildOnly: true,
	permission: 'ADMINISTRATOR',
	adminOverride: true,
	description: 'Mistake statistics for 365 days',
	run: async (message: Message) => {
		await statsUtils.replyWithStatistics(message, 365)
	}
}
