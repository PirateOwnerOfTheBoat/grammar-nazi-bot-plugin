import { Command } from '../../types/Command'
import { Message } from 'discord.js'
import grammarUtils from '../../utils/grammarUtils'

export const RunChecks: Command = {
	name: 'runchecks',
	args: 1,
	guildOnly: true,
	permission: 'ADMINISTRATOR',
	adminOverride: true,
	description: 'Checks x messages in every channel for gramatical messages',
	usage: '<messageCount>',
	run: async (message: Message, args: string[]) => {
		// const messages = await grammarUtils.loadMessages(message.channel as TextChannel, parseInt(args[0]))

		//@ts-expect-error guildOnly command
		await grammarUtils.checkGuild(message.guild, parseInt(args[0]))
		await message.reply({ content: 'Checked messages in all channels.' })
	}
}
