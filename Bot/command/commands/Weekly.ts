import { Message } from 'discord.js'
import { Command } from '../../types/Command'
import statsUtils from '../../utils/statsUtils'

export const Weekly: Command = {
	name: 'weekly',
	args: 0,
	guildOnly: true,
	permission: 'ADMINISTRATOR',
	adminOverride: true,
	description: 'Mistake statistics for 7 days',
	run: async (message: Message) => {
		await statsUtils.replyWithStatistics(message, 7)
	}
}
