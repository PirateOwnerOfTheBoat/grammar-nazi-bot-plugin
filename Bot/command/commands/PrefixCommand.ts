import Prefix from '@/Misko/NaziBot/Models/Prefix'
import { Message } from 'discord.js'
import { Command } from '../../types/Command'

export const PrefixCommand: Command = {
	name: 'prefix',
	args: 1,
	guildOnly: true,
	permission: 'ADMINISTRATOR',
	adminOverride: true,
	description: 'Sets guild prefix',
	usage: '<prefix>',
	run: async (message: Message, args: string[]) => {
		// If guild is still using default prefix create new prefix
		// or else update already existing prefix
		// @ts-expect-error checked in messageCreateEvent
		await Prefix.updateOrCreate({ guildId: message.guildId }, { prefix: args[0] })

		await message.reply({ content: `Prefix changed to \`${args[0]}\`.` })
	}
}
