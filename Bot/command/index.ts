import { Command } from '../types/Command'
import { LogChannelCommand } from './commands/LogChannelCommand'
import { PrefixCommand } from './commands/PrefixCommand'
import { GrammarEmojiCommand } from './commands/GrammarEmojiCommand'
import { NaziRule } from './commands/NaziRule'
import { RunChecks } from './commands/RunChecks'
import { Weekly } from './commands/Weekly'
import { Monthly } from './commands/Monthly'
import { Yearly } from './commands/Yearly'
import { Help } from './commands/Help'

export const commands: Command[] = [
	PrefixCommand,
	LogChannelCommand,
	GrammarEmojiCommand,
	NaziRule,
	RunChecks,
	Weekly,
	Monthly,
	Yearly,
	Help
]
