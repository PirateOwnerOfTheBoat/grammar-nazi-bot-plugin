import { Message } from 'discord.js'
import arr from './mistakes.json'
import fs from 'fs'

function prepare(s: string): string {
	return s
		.normalize('NFD')
		.replace(/[0-9]|[^\w\s]|[\u0300-\u036f]/g, '')
		.toLowerCase()
}

// function checkNazi(s: string): string | undefined {
// 	const out = []

// 	for (let i = 0; i < arr.length; i++) {
// 		const a = arr[i]

// 		const b = _findMatches(s, a.q)
// 		if (b) {
// 			out.push(a.note as never)
// 		}
// 	}
// 	if (out.length) {
// 		return out.join(', ')
// 	}
// }

// function _findMatches(s: string, array: string[]): boolean {
// 	for (let i = 0; i < array.length; i++) {
// 		const a = prepare(array[i])

// 		const re = new RegExp('\\b' + a + '\\b', 'i')
// 		if (re.test(s)) {
// 			return true
// 		}
// 	}
// 	return false
// }

function checkNazi(s: string): string | undefined {
	const out = []

	for (let i = 0; i < arr.length; i++) {
		const a = arr[i]

		const re = new RegExp('\\b' + a.mistake + '\\b', 'i')

		if (re.test(s)) {
			//@ts-expect-error
			out.push(a.category)
		}
	}

	if (out.length) {
		return out.join(', ')
	}
}

function checkSuggestions(s: string): string | undefined {
	const suggestions = JSON.parse(
		fs.readFileSync('plugins/Misko/NaziBot/Bot/suggestions.json', { encoding: 'utf-8', flag: 'r' })
	)
	const out = []

	for (let i = 0; i < suggestions.length; i++) {
		const a = suggestions[i]

		const re = new RegExp('\\b' + a.mistake + '\\b', 'i')

		if (re.test(s)) {
			//@ts-expect-error
			out.push(a.category)
		}

		if (out.length) {
			return out.join(', ')
		}
	}
}

export default {
	checkMessage(message: Message): string | undefined {
		return checkNazi(prepare(message.content))
	},
	checkSuggestions(message: Message): string | undefined {
		return checkSuggestions(prepare(message.content))
	}
}
