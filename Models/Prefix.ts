import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Prefix extends BaseModel {
	public static table = 'misko_nazibot_prefixes'

	@column({ isPrimary: true })
	public id: number

	@column()
	public guildId: string

	@column()
	public prefix: string

	@column.dateTime({ autoCreate: true })
	public createdAt: DateTime

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	public updatedAt: DateTime
}
