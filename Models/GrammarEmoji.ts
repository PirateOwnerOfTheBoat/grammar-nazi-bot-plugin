import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class GrammarEmoji extends BaseModel {
	public static table = 'misko_nazibot_grammaremojis'

	@column({ isPrimary: true })
	public id: number

	@column()
	public guildId: string

	@column()
	public emojiId: string

	@column.dateTime({ autoCreate: true })
	public createdAt: DateTime

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	public updatedAt: DateTime
}
