import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Mistake extends BaseModel {
	public static table = 'misko_nazibot_mistakes'

	@column({ isPrimary: true })
	public id: number

	@column()
	public guildId: string

	@column()
	public messageId: string

	@column()
	public authorId: string

	@column()
	public messageContent: string

	@column()
	public mistake: string

	@column()
	public channelId: string

	@column.dateTime()
	public messageTimestamp: DateTime

	@column.dateTime({ autoCreate: true })
	public createdAt: DateTime

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	public updatedAt: DateTime
}
