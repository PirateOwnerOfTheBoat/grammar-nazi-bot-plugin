import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Report extends BaseModel {
	public static table = 'misko_nazibot_reports'

	@column({ isPrimary: true })
	public id: number

	@column()
	public messageContent: string

	@column()
	public authorId: string

	@column()
	public userId: string

	@column()
	public guildId: string

	@column()
	public messageId: string

	@column()
	public channelId: string

	@column.dateTime({ autoCreate: true })
	public createdAt: DateTime

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	public updatedAt: DateTime
}
