import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CreateGrammarEmojisTable extends BaseSchema {
	protected tableName = 'misko_nazibot_grammaremojis'

	public async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.increments('id')

			table.string('guild_id').unique().notNullable().index()
			table.string('emoji_id').notNullable()

			/**
			 * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
			 */
			table.timestamp('created_at', { useTz: true })
			table.timestamp('updated_at', { useTz: true })
		})
	}

	public async down() {
		this.schema.dropTable(this.tableName)
	}
}
