import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CreateMistakesTable extends BaseSchema {
	protected tableName = 'misko_nazibot_mistakes'

	public async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.increments('id')

			table.string('guild_id').nullable()
			table.string('channel_id').nullable()
			table.string('message_id').unique().notNullable()
			table.string('author_id').notNullable()
			table.string('message_content').notNullable()
			table.string('mistake').notNullable()
			table.timestamp('message_timestamp').notNullable()

			/**
			 * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
			 */
			table.timestamp('created_at', { useTz: true })
			table.timestamp('updated_at', { useTz: true })
		})
	}

	public async down() {
		this.schema.dropTable(this.tableName)
	}
}
