import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CreateReportsTable extends BaseSchema {
	protected tableName = 'misko_nazibot_reports'

	public async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.increments('id')

			table.string('message_content').notNullable()
			table.string('guild_id').notNullable()
			table.string('channel_id').notNullable()
			table.string('author_id').notNullable()
			table.string('user_id').notNullable()
			table.string('message_id').notNullable().unique()

			/**
			 * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
			 */
			table.timestamp('created_at', { useTz: true })
			table.timestamp('updated_at', { useTz: true })
		})
	}

	public async down() {
		this.schema.dropTable(this.tableName)
	}
}
