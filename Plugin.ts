import PluginBase from '@wezeo/cmf-plugins/src/PluginBase'
import { token } from './Bot/config.json'

export class Plugin extends PluginBase {
	async register() {
		//
	}

	async boot() {
		//
	}

	async ready() {
		const client = (await import('./Bot')).default

		client.login(token)
	}
}
